organization := "no.uit.sfb"
name := "sbt-git-smartversion"
licenses := Seq(("BSD-2-Clause", url("https://opensource.org/licenses/BSD-2-Clause")))
description := "An sbt plugin that offers git features directly inside sbt"
scalaVersion := "2.12.8"

credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

publishTo := {
  if (version.value.endsWith("-SNAPSHOT"))
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local;build.timestamp=" + new java.util.Date().getTime)
  else
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local")
}

enablePlugins(GitVersioning, SbtPlugin)
useJGit
git.gitlabCiOverride := true
//Override the targetVersionFile here if needed
//git.targetVersionFile := "./anotherFile"

libraryDependencies ++= Seq(
  "org.eclipse.jgit" % "org.eclipse.jgit" % "4.9.0.201710071750-r",
  "com.michaelpollmeier" % "versionsort" % "1.0.0",
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
)

scriptedLaunchOpts += s"-Dproject.version=${version.value}"

enablePlugins(BuildInfoPlugin)
buildInfoKeys := Seq[BuildInfoKey](
  name,
  version,
  scalaVersion,
  sbtVersion,
  git.gitSha,
  git.targetVersion,
  git.gitCurrentBranch,
  git.gitCurrentTags,
  git.targetVersionFile,
  git.versions
)
buildInfoPackage := s"${organization.value}.info.${name.value.replace('-', '_')}"
