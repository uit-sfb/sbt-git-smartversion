# sbt-git-smartversion

The `sbt-git-smartversion` plugin is a quick and dirty remix of the excellent and original [git-sbt](https://github.com/marcadella/sbt-git) plugin.
Everything is the same except the `version` that is overridden with a custom mechanism described below.

## Installation

This plugin requires at least **Java 8**.

- In your project's `project/build.sbt` (create it if it does not exist) add:  
  `resolvers +=  "Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"`
- In your project's `project/plugins.sbt` (create it if it does not exist) add:  
  `addSbtPlugin("no.uit.sfb" % "sbt-git-smartversion" % "1.0.0")`
- To activate the plugin, add `enablePlugins(GitVersioning)` to your `.sbt` file.

Note that this project is using `sbt-git-smartversion` plugin itself, so it can be used as an example.

### Using JGit

If you do not have git installed and available on your path (e.g. you use windows),
make sure your `.sbt` file contains `useJGit`

This will enable a java-only GIT solution that, while not supporting all the same
commands that can be run in the standard git command line, is good enough for most
git activities.


## Versioning with Git

The versioning mechanism is completely different from [git-sbt](https://github.com/sbt/sbt-git).
The following sections describe how versioning is handled by this plugin.

### Version list

A "target version" is the version the team is aiming at. It should be like "0.22.4" (no "-SNAPSHOT" or other suffix).
This target version is defined by default in `./targetVersion` but it is possible to change the location by setting `git.targetVersionFile := "some/other/path"` in your `.sbt` file.

The plugin defines a list of versions based on the target version, the branch and the git tags present on `HEAD` as follows:
  - if `HEAD` has a branch defined: `<targetVersion>-<branchName>-SNAPSHOT` (if branch is "master" then it is: `<targetVersion>-SNAPSHOT`)
  - for each git tag on `HEAD` that is a version value `ver` = `x.y.z[-rc<n>]` (where `rc` stands for release candidate): `ver` 
  In addition, check if the target version file has been updated to contain an increment of `ver`.
  - for each git tag on `HEAD` that is not a version value: `<targetVersion>-<tag>-SNAPSHOT`

To print the list of versions: `sbt lsver`.

Notes:
  - Each time the target version file is changed, a git tag is added/removed, or a commit is done, `sbt reload` must be executed to update the project version.
  - When working with gitlab-ci, set `git.gitlabCiOverride := true` which overrides the branch, commit sha and commit tag with values defined
by Gitlab CI's environment variables (if the execution environment is recognized as being Gitlab CI).
In addition, in GitlabCi, during a tag commit, the branch in not known which means that the version will not contain the branch name.

### Version selection

Each version of the version list is a valid possible version one may want to use to publish artifacts for instance.

Given this list of versions:
  - `version` is set to the first item of the list (i.e. the branch version if defined or one of the tag versions),
  - to change version: `sbt selver <x>` where `<x>` is an integer designating a version (cf `sbt lsver`).

### Co-working with non-SBT projects

When the project is composed of SBT and non-SBT projects, one way to make the non-SBT projects aware of the version decided by this plugin is to write it in a file
that act as source of truth for the whole project.
This can be achieved by adding those lines to the top level `build.sbt`:
```sbt
val updateDeploymentVersions = SettingKey[Unit](
  "update-deployment-version",
  "Update the deployment version")

updateDeploymentVersions := {
  val pw = new PrintWriter(new File(".version"))
  pw.write(version.value)
  pw.close()
}
```

which prints the version into `.version` each time the top level project is reloaded in SBT.

## Other features

For all the other features, please have a look at the [git-sbt](https://github.com/marcadella/sbt-git).

## Licensing

This software is licensed under the BSD license.
