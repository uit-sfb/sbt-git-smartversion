resolvers ++= Seq(
  "Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/",
  "Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local/"
)
