package no.uit.sfb.sbt.smartversion

case class ProtectedVersion(major: Int,
                            minor: Int,
                            patch: Int,
                            rc: Option[Int] = None) {
  override def toString = {
    s"$major.$minor.$patch${rc
      .map { n =>
        s"-rc$n"
      }
      .getOrElse("")}"
  }

  def isRc(): Boolean = rc.isDefined

  def hasNext(other: ProtectedVersion): Boolean = {
    val (v1, v2, v3) =
      (major + 1, minor + 1, patch + 1)
    other match {
      case ProtectedVersion(`v1`, 0, 0, _)             => true
      case ProtectedVersion(`major`, `v2`, 0, _)       => true
      case ProtectedVersion(`major`, `minor`, `v3`, _) => true
      case _                                           => false
    }
  }
}

object ProtectedVersion {
  def fromString(str: String,
                 allowRc: Boolean = true): Option[ProtectedVersion] = {
    val regexp = """^(\d+)\.(\d+)\.(\d+)(?:-rc(\d+))?$""".r
    str match {
      case regexp(v1, v2, v3, orc) =>
        if (allowRc || orc == null)
          Some(ProtectedVersion(v1.toInt, v2.toInt, v3.toInt, Option(orc).map {
            _.toInt
          }))
        else
          None
      case _ => None
    }
  }
}
