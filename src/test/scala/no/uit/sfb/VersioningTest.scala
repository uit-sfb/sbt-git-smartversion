package no.uit.sfb

import org.scalatest.{FunSpec, Matchers}
import no.uit.sfb.info.sbt_git_smartversion.BuildInfo
import no.uit.sfb.sbt.smartversion.ProtectedVersion

class VersioningTest extends FunSpec with Matchers {
  println(s"Version: ${BuildInfo.version}")
  println(s"SHA: ${BuildInfo.gitSha}")
  println(s"TargetVersionFile: ${BuildInfo.targetVersionFile}")
  println(s"TargetVersion: ${BuildInfo.targetVersion}")
  println(s"Branch: ${BuildInfo.gitCurrentBranch}")
  println(s"Tags: ${BuildInfo.gitCurrentTags}")
  println(s"Versions: ${BuildInfo.versions}")

  describe("version pattern") {
    it("should match 10.22.3") {
      ProtectedVersion.fromString("10.22.3") should be(
        Some(ProtectedVersion(10, 22, 3, None)))
    }
    it("should match 10.22.3-rc2") {
      ProtectedVersion.fromString("10.22.3-rc2") should be(
        Some(ProtectedVersion(10, 22, 3, Some(2))))
    }
    it("should not match 10.22.3-rc2 when allowRc is false") {
      ProtectedVersion.fromString("10.22.3-rc2", false) should be(None)
    }
    it("should not match 10.22.3-rc2-mytag") {
      ProtectedVersion.fromString("10.22.3-rc2-mytag") should be(None)
    }
    it("should not match 10.22.3-mytag-rc2") {
      ProtectedVersion.fromString("10.22.3-mytag-rc2") should be(None)
    }
    it("should not match 10.22.3-SNAPSHOT") {
      ProtectedVersion.fromString("10.22.3-SNAPSHOT") should be(None)
    }
    it("should not match 10.22") {
      ProtectedVersion.fromString("10.22") should be(None)
    }
  }
  describe("git.consecutive") {
    it("should recognize that 1.2.3 follows 1.2.2") {
      ProtectedVersion(1, 2, 2).hasNext(ProtectedVersion(1, 2, 3)) should be(
        true)
    }
    it("should recognize that 1.2.2 does not follow 1.2.2") {
      ProtectedVersion(1, 2, 2).hasNext(ProtectedVersion(1, 2, 2)) should be(
        false)
    }
    it("should recognize that 2.2.2 does not follow 1.2.2") {
      ProtectedVersion(1, 2, 2).hasNext(ProtectedVersion(2, 2, 2)) should be(
        false)
    }
  }
}
